package trees;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {

    Node root;
    private String travel = "";

    public String getTravel() {
        return travel;
    }

    public void cleanTravel() {
        this.travel = "";
    }

    private Node addRecursive(Node current, int value) {
        if (current == null) {
            return new Node(value);
        }

        if (value < current.value) {
            current.left = addRecursive(current.left, value);
        } else if (value > current.value) {
            current.right = addRecursive(current.right, value);
        } else {
            // value already exists
            return current;
        }

        return current;
    }

    public void add(int value) {
        root = addRecursive(root, value);
    }

    private boolean containsNodeRecursive(Node current, int value) {
        if (current == null) {
            return false;
        }
        if (value == current.value) {
            return true;
        }
        return value < current.value
                ? containsNodeRecursive(current.left, value)
                : containsNodeRecursive(current.right, value);
    }

    public boolean containsNode(int value) {
        return containsNodeRecursive(root, value);
    }

    private Node deleteRecursive(Node current, int value) {
        if (current == null) {
            return null;
        }

        if (value == current.value) {
            if (current.left == null && current.right == null) {
                return null;
            }
            if (current.right == null) {
                return current.left;
            }

            if (current.left == null) {
                return current.right;
            }

        }
        if (value < current.value) {
            current.left = deleteRecursive(current.left, value);
            return current;
        }
        current.right = deleteRecursive(current.right, value);
        return current;
    }

    private int findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }

    public void delete(int value) {
        root = deleteRecursive(root, value);
    }

    public String traverseInOrder(Node node) {
        if (node != null) {
            traverseInOrder(node.left);
            //System.out.print(" " + node.value);
            travel += " " + node.value;
            traverseInOrder(node.right);
        }
        return travel;
    }

    public String traversePreOrder(Node node) {
        if (node != null) {
            //System.out.print(" " + node.value);
            travel += " " + node.value;
            traversePreOrder(node.left);
            traversePreOrder(node.right);
        }
        return travel;
    }

    public String traversePostOrder(Node node) {
        if (node != null) {
            traversePostOrder(node.left);
            traversePostOrder(node.right);
            //System.out.print(" " + node.value);
            travel += " " + node.value;
        }
        return travel;
    }

    public boolean isEmpty() {
        return root == null;
    }
}
